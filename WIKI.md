# Architecture

## Model-View-Presenter

Model-View-Presenter (MVP) pattern should be used in all new projects.
The MVP pattern consists of three layers:
- Model
Contains all relevant enteties (e. g. _ViewModels_, _DataModels_, _Providers_, _Factories_)
- Presentation layer
Is considered a proxy between a _Model_ and a _View_ layer. Handles all interactions from the view (e. g. clicks, item selections, text input) and is responsible for updating the _View_ with the current _Model_ state.

- View layer
Contains all of UI elements and is a representation of the current state of the _Model_

# Package naming

### Main app packages
All project should contain two typical packages:
```
├── main/
│   ├── common
│   ├── core
```

- `common` 
This package contains fundamental architecure base classes and interfaces, used through the Team. This classes should not be altered. If customisation requiremant then the file should be moved to 'core' or should also be updated in `AppCore` project.

- `core`
This is a set of classes that is used only locally (per project) in various places.
Usually should contain common interfaces and base classes used explicitly in particular project

### Feature packages

Other packages should be organised according to a particular feature or functionality (e. g. `signin`, `account`) and contain all the classes necessary to deliver particular functionality unless their are also used or needed in other parts of the app (e. g. validators, formatters, common view interfaces, views). In such case they should be placed in `core` package.

**Example app structure:**
```
├── main/
│   ├── common
│   ├── core
│   ├── contacts
│   ├── cccount
│   ├── signin
│   ├── signup
│   ├── navigation
```

### Feature package content


**Example feature package content:**
```
├── main/
│   ├── common
│   ├── core
│   ├── contacts
│   ├── caccount
│   ├── signin
│   ├── signup
│   │   ├── presentation
│   │   ├── models
│   │   ├── ui
│   │   ├── helpers
```

- `<feature>/presentation` contains all *view* presentation logic that is:
presenters, adapters

- `<feature>/models` contains all *view* models and any other used explicitly on the view

- `<feature>/ui` contains all gui related classes like e. g. `View`, `Activity`, `Fragment`, `Dialog`. The components can be reorganised to sub-packages
- `<feature>/helpers` contains all helper classes such as formatters, validators etc.

# Class and field naming naming
### Variables

# Resources

### Color naming convention

##### App colors rules
App pallete calors (colors used throught the app) should be defined with an app (shortcut as a prefix then followed with the color name ) 

**Example:**
`<color name="cccs_red">#ff0000</color>`

All feature/view specific colors should be defined as follow:
`<feature_name>_<component_name>_<state_name>`
with `<state_name>` beeing optional (if needed).
**Example**
`<color name="sign_up_email_input_bg_focused">#00ff00</color>`

Whenever possible try to reuse colors from the pallete:
**Example**
`<color name="account_edit_submit_btn_text">@color/wbw_green</color>`

##### Distinguishing colors in names
Try to make color names as readable as possible, which means giving as much informations about the color in the name as possible. In ideal situation the name  of the color should give out enough info to prevent you from going to `colors` resources.

Usually the app contains at least few grayscale colors with different transaprancy levels. Following rules can be applied while naming:
- use hex in color names e. g. `#cccccc` should be named `tsh_gray_cc`, `#e2e2e2` - `inc_gray_e2`
- indicate that color is `argb` with `<color_name>_transparent_<xx>` where `<xx>` indicate the levelo fo transparency (0-100, as it is the easiest to read and most of graphic programs use this scale) e. g. `#80ffffff` should be named `zxcv_white_transparent_50`

### Drawables naming convention
##### General
Try to avoid using color names in drawable names whenever possible, as it might require file refactoring when the design changes. Instead drawable name should reference the feature that it is used in and/or the component.

**Bad**
`/drawables/btn_green_pressed.xml`
**Better**
`/drawable/btn_show_details_pressed.xml`


##### Prefixes
Try to use prefixes when defining drawables:
- `btn` for `Button`,
- `et` for `EditText`,
- `ic` for icons,
- `img` for images (hero images, backgrounds),
- `item` list items,
- `tab` tabs.

**Examples:**
`drawable/item_contacts_list_item_title.xml`
`drawable/btn_new_contact_submit_normal.xml`
`drawable/img_drawer_user_avatar_default.xml`
`drawable/ic_contact_details_favorite.xml` 

##### Naming rules
Drawable names should be created according to following scheme:
`<component>_<feature>_<ui_function>`
Example:
`btn_sign_in_submit`
`et_account_email_label`

##### State drawables

When drawable has many states following scheme should be applied
`<component>_<feature>_<ui_function>_<state>`,
then the drawable with selector should not have `_<state>` bit
**Example:**
```
├── drawables/
│   ├── et_payment_form__redit_card_number_focused.xml
│   ├── et_payment_form_credit_card_number_pressed_focused.xml
│   ├── et_payment_form_credit_card_number_pressed.xml
│   ├── et_payment_form_credit_card_number_fnormal.xml
│   ├── et_payment_form_credit_card_number.xml
```

## Layout resources naming
### General
Just like any other resource, layout resource name should clearly state to which feature does the layout belong to and what does it do (or what is responsible for, so that other devs can easily find and determine layout usage without clicking through the classes.

### Naming convention
#### Suffixes

`activity_<feature_name>` `Activity` content,
`fragment_<ui_fragment_name>` `Fragment` layout definictions,
`dialog_<dialog_name>` any `Dialog` or `DialogFragment` represntation,
`item_<item_name>` list items layouts,
`layout_<layout_name>`any part of layout that is reused in differents parts of the app (e. g. `<merge></merge>`, `ViewStubs` contents etc.),
`view_<view_name>` custom `View` layout.

# Tools
This are android libraries used through projects:
- Butterknife
- RxJava
- Retrofit
- Realm 
- Dagger
- Hawk
- FastAdapter
- FlexibleAdapter


# Git
### Commits
##### General rules
Commit message should fully explain the changes that were made in project and/or the impact it had on working functionality. In th same time try to keep the commit messages as short as possible (one sentence, one line)
**Bad:**
`Fixed bugs`
**Good:**
`Fixed app crash on logout when no internet connection`

##### Naming convention
Try to name your commit in a way to let non-technical, non-android-developer figure out exacly what you did. Start commit name with capital letter so it looks like a normal sentence.
**Bad:**
`added SignInActivity and SignInPresenter`
**Good:**
`Basic sign in screen set up.`
##### Workflow
Try to have as much commits in your app repository and keep as little changes per commit as possible. The commit flow should describe the step by step how you conducted with task execution.
Every commit should contain working and compilable project.

### Feature branches
##### General rules
Every feature branch should correspond to a task in project manage tool (e. g. Jira).
##### Naming conventions
Every feature branch name should follow this scheme:
`feature/<project_prefix>-<task_nr>-<task_description>`
**Example:**
`feature/CCCS-23-Reset-password`

### Release naming

### Proguard
### References
[ribot Android Guidelines from github](https://github.com/ribot/android-guidelines)