package tsh.blank.views;

import android.content.Context;

import tsh.blank.injection.annotation.ContextActivity;
import tsh.blank.injection.annotation.PerActivity;
import tsh.blank.views.dialog.ProgressDialog;

import javax.inject.Inject;

@PerActivity
public class SavingDialog extends ProgressDialog {

    @Inject
    public SavingDialog(@ContextActivity Context context) {
        super(context);

        setContent("Saving changes");
        setNegativeVisible(false);
        setCancelable(false);
    }
}
