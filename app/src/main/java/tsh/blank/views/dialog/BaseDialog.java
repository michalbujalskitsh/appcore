package tsh.blank.views.dialog;

import android.content.Context;
import android.support.annotation.StringRes;
import android.view.View;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.rafakob.utils.holder.StringHolder;

public abstract class BaseDialog {
    protected Context context;
    protected MaterialDialog dialog;

    protected StringHolder title = new StringHolder(null);
    protected StringHolder content = new StringHolder(null);
    protected StringHolder positiveText = new StringHolder(null);
    protected StringHolder negativeText = new StringHolder(null);
    protected StringHolder neutralText = new StringHolder(null);

    protected PositiveListener positiveListener;
    protected NegativeListener negativeListener;
    protected NeutralListener neutralListener;
    protected DismissListener dismissListener;

    protected boolean isPositiveVisible;
    protected boolean isNegativeVisible;
    protected boolean isNeutralVisible;
    protected boolean isPositiveEnabled = true;
    protected boolean isNegativeEnabled = true;
    protected boolean isNeutralEnabled = true;
    protected boolean isCancelable = true;

    public BaseDialog(Context context) {
        this.context = context;
    }

    public void setTitle(String title) {
        this.title = new StringHolder(title);
        if (dialog != null) {
            dialog.setTitle(title);
        }
    }

    public void setTitle(@StringRes int title) {
        this.title = new StringHolder(title);
        if (dialog != null) {
            dialog.setTitle(title);
        }
    }

    public void setContent(String content) {
        this.content = new StringHolder(content);
        if (dialog != null) {
            dialog.setContent(content);
        }
    }

    public void setContent(@StringRes int content) {
        this.content = new StringHolder(content);
        if (dialog != null) {
            dialog.setContent(content);
        }
    }

    public void onPositive(PositiveListener positiveListener) {
        this.positiveListener = positiveListener;
    }

    public void onNegative(NegativeListener negativeListener) {
        this.negativeListener = negativeListener;
    }

    public void onNeutral(NeutralListener neutralListener) {
        this.neutralListener = neutralListener;
    }

    public void onDismiss(DismissListener dismissListener) {
        this.dismissListener = dismissListener;
    }

    public void setPositiveVisible(boolean visible) {
        isPositiveVisible = visible;
        if (dialog != null) {
            dialog.getActionButton(DialogAction.POSITIVE).setVisibility(visible ? View.VISIBLE : View.GONE);
        }
    }

    public void setNegativeVisible(boolean visible) {
        isNegativeVisible = visible;
        if (dialog != null) {
            dialog.getActionButton(DialogAction.NEGATIVE).setVisibility(visible ? View.VISIBLE : View.GONE);
        }
    }

    public void setNeutralVisible(boolean visible) {
        isNeutralVisible = visible;
        if (dialog != null) {
            dialog.getActionButton(DialogAction.NEUTRAL).setVisibility(visible ? View.VISIBLE : View.GONE);
        }
    }

    public void setPositiveEnabled(boolean enabled) {
        isPositiveEnabled = enabled;
        if (dialog != null) {
            dialog.getActionButton(DialogAction.POSITIVE).setEnabled(enabled);
        }
    }

    public void setNegativeEnabled(boolean enabled) {
        isNegativeEnabled = enabled;
        if (dialog != null) {
            dialog.getActionButton(DialogAction.NEGATIVE).setEnabled(enabled);
        }
    }

    public void setNeutralEnabled(boolean enabled) {
        isNeutralEnabled = enabled;
        if (dialog != null) {
            dialog.getActionButton(DialogAction.NEUTRAL).setEnabled(enabled);
        }
    }

    public void setPositiveText(String text) {
        positiveText = new StringHolder(text);
        if (dialog != null) {
            dialog.getActionButton(DialogAction.POSITIVE).setText(text);
        }
    }

    public void setPositiveText(@StringRes int text) {
        positiveText = new StringHolder(text);
        if (dialog != null) {
            dialog.getActionButton(DialogAction.POSITIVE).setText(text);
        }
    }

    public void setNegativeText(String text) {
        negativeText = new StringHolder(text);
        if (dialog != null) {
            dialog.getActionButton(DialogAction.NEGATIVE).setText(text);
        }
    }

    public void setNegativeText(@StringRes int text) {
        negativeText = new StringHolder(text);
        if (dialog != null) {
            dialog.getActionButton(DialogAction.NEGATIVE).setText(text);
        }
    }

    public void setNeutralText(String text) {
        neutralText = new StringHolder(text);
        if (dialog != null) {
            dialog.getActionButton(DialogAction.NEUTRAL).setText(text);
        }
    }

    public void setNeutralText(@StringRes int text) {
        neutralText = new StringHolder(text);
        if (dialog != null) {
            dialog.getActionButton(DialogAction.NEUTRAL).setText(text);
        }
    }

    public void setCancelable(boolean cancelable) {
        isCancelable = cancelable;
        if (dialog != null) {
            dialog.setCancelable(cancelable);
        }
    }

    public boolean isShowing() {
        return dialog != null && dialog.isShowing();
    }

    public void show() {
        if (dialog == null) {
            build();
        }

        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    public void dismiss() {
        if (isShowing()) {
            dialog.dismiss();
        }
    }

    protected abstract void build();

    protected void buildDefault() {
        dialog = new MaterialDialog.Builder(context)
                .title(title.getText(context))
                .content(content.getText(context))
                .neutralText(neutralText.getText(context))
                .negativeText(negativeText.getText(context))
                .positiveText(positiveText.getText(context))
                .onPositive((dialog, which) -> {
                    if (positiveListener != null) {
                        positiveListener.onPositive();
                    }
                })
                .onNegative((dialog, which) -> {
                    if (negativeListener != null) {
                        negativeListener.onNegative();
                    }
                })
                .onNeutral((dialog, which) -> {
                    if (neutralListener != null) {
                        neutralListener.onNeutral();
                    }
                })
                .dismissListener(dialog -> {
                    if (dismissListener != null) {
                        dismissListener.onDismiss();
                    }
                })
                .cancelable(isCancelable)
                .build();

        updateButtons();
    }

    protected final void updateButtons() {
        dialog.getActionButton(DialogAction.POSITIVE).setVisibility(isPositiveVisible ? View.VISIBLE : View.GONE);
        dialog.getActionButton(DialogAction.NEGATIVE).setVisibility(isNegativeVisible ? View.VISIBLE : View.GONE);
        dialog.getActionButton(DialogAction.NEUTRAL).setVisibility(isNeutralVisible ? View.VISIBLE : View.GONE);

        dialog.getActionButton(DialogAction.POSITIVE).setEnabled(isPositiveEnabled);
        dialog.getActionButton(DialogAction.NEGATIVE).setEnabled(isNegativeEnabled);
        dialog.getActionButton(DialogAction.NEUTRAL).setEnabled(isNeutralEnabled);


        dialog.getActionButton(DialogAction.POSITIVE).setText(positiveText.getText(context));
        dialog.getActionButton(DialogAction.NEGATIVE).setText(negativeText.getText(context));
        dialog.getActionButton(DialogAction.NEUTRAL).setText(neutralText.getText(context));
    }

    public interface PositiveListener {
        void onPositive();
    }

    public interface NegativeListener {
        void onNegative();
    }

    public interface NeutralListener {
        void onNeutral();
    }

    public interface DismissListener {
        void onDismiss();
    }
}
