package tsh.blank.views.dialog;

import android.content.Context;

import tsh.blank.injection.annotation.ContextActivity;
import tsh.blank.injection.annotation.PerActivity;

import javax.inject.Inject;

@PerActivity
public class ConfirmDialog extends BaseDialog {
    @Inject
    public ConfirmDialog(@ContextActivity Context context) {
        super(context);
        title.setText("Confirm");
        content.setText("stub");
        negativeText.setText("NO");
        positiveText.setText("YES");
        isNeutralVisible = false;
        isNegativeVisible = true;
        isPositiveVisible = true;
        isCancelable = true;
    }

    @Override
    protected void build() {
        buildDefault();
    }
}
