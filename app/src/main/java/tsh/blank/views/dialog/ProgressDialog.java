package tsh.blank.views.dialog;

import android.content.Context;
import android.view.View;

import com.afollestad.materialdialogs.MaterialDialog;
import tsh.blank.injection.annotation.ContextActivity;
import tsh.blank.injection.annotation.PerActivity;
import com.rafakob.utils.holder.StringHolder;

import javax.inject.Inject;


@PerActivity
public class ProgressDialog extends BaseDialog {
    private boolean isProgressEnabled = true;

    @Inject
    public ProgressDialog(@ContextActivity Context context) {
        super(context);
        negativeText = new StringHolder("CANCEL");
        positiveText = new StringHolder("OK");
        isNeutralVisible = false;
        isNegativeVisible = true;
        isPositiveVisible = false;
    }

    @Override
    protected void build() {
        dialog = new MaterialDialog.Builder(context)
                .title(title.getText(context))
                .content(content.getText(context))
                .neutralText(neutralText.getText(context))
                .negativeText(negativeText.getText(context))
                .positiveText(positiveText.getText(context))
                .progress(true, 0)
                .progressIndeterminateStyle(true)
                .cancelable(isCancelable)
                .onPositive((dialog, which) -> {
                    if (positiveListener != null) {
                        positiveListener.onPositive();
                    }
                })
                .onNegative((dialog, which) -> {
                    if (negativeListener != null) {
                        negativeListener.onNegative();
                    }
                })
                .onNeutral((dialog, which) -> {
                    if (neutralListener != null) {
                        neutralListener.onNeutral();
                    }
                })
                .dismissListener(dialog -> {
                    if (dismissListener != null) {
                        dismissListener.onDismiss();
                    }
                })
                .cancelable(isCancelable)
                .build();

        setProgressEnabled(isProgressEnabled);
        updateButtons();
    }

    public void setProgressEnabled(boolean enabled) {
        isProgressEnabled = enabled;
        if (dialog != null) {
            dialog.getProgressBar().setVisibility(enabled ? View.VISIBLE : View.GONE);
        }
    }
}