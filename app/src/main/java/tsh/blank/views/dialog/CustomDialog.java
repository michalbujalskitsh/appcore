package tsh.blank.views.dialog;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.view.View;

import com.afollestad.materialdialogs.MaterialDialog;
import tsh.blank.injection.annotation.ContextActivity;
import tsh.blank.injection.annotation.PerActivity;

import javax.inject.Inject;

@PerActivity
public class CustomDialog extends BaseDialog {
    private int customView;
    private boolean wrapInScroll;
    private View.OnClickListener onClickListener;

    @Inject
    public CustomDialog(@ContextActivity Context context) {
        super(context);
        isNeutralVisible = false;
        isNegativeVisible = false;
        isPositiveVisible = false;
    }

    @Override
    protected void build() {
        dialog = new MaterialDialog.Builder(context)
                .title(title.getText(context))
                .content(content.getText(context))
                .neutralText(neutralText.getText(context))
                .negativeText(negativeText.getText(context))
                .positiveText(positiveText.getText(context))
                .customView(customView, wrapInScroll)
                .onPositive((dialog, which) -> {
                    if (positiveListener != null) {
                        positiveListener.onPositive();
                    }
                })
                .onNegative((dialog, which) -> {
                    if (negativeListener != null) {
                        negativeListener.onNegative();
                    }
                })
                .onNeutral((dialog, which) -> {
                    if (neutralListener != null) {
                        neutralListener.onNeutral();
                    }
                })
                .dismissListener(dialog -> {
                    if (dismissListener != null) {
                        dismissListener.onDismiss();
                    }
                })
                .build();

        dialog.getCustomView().setOnClickListener(onClickListener);
        updateButtons();
    }

    @Override
    public void show() {
        super.show();
        onViewCreated(dialog.getCustomView());
    }

    protected void onViewCreated(View view) {
    }

    public void onClickListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public void setCustomView(@LayoutRes int customView) {
        this.customView = customView;
    }

    public void setWrapInScroll(boolean wrapInScroll) {
        this.wrapInScroll = wrapInScroll;
    }
}
