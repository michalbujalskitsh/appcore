package tsh.blank.views.dialog;

import android.content.Context;
import android.support.annotation.ArrayRes;
import android.view.View;

import com.afollestad.materialdialogs.MaterialDialog;
import tsh.blank.injection.annotation.ContextActivity;
import tsh.blank.injection.annotation.PerActivity;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

@PerActivity
public class SimpleListDialog extends BaseDialog {
    private List<CharSequence> items = new LinkedList<>();
    private Listener listener;
    private boolean autoClose;


    @Inject
    public SimpleListDialog(@ContextActivity Context context) {
        super(context);
        title.setText("Confirm");
        negativeText.setText("NO");
        positiveText.setText("YES");
        isNeutralVisible = false;
        isNegativeVisible = false;
        isPositiveVisible = false;
    }

    @Override
    protected void build() {
        dialog = new MaterialDialog.Builder(context)
                .title(title.getText(context))
                .content(content.getText(context))
                .neutralText(neutralText.getText(context))
                .negativeText(negativeText.getText(context))
                .positiveText(positiveText.getText(context))
                .onPositive((dialog, which) -> {
                    if (positiveListener != null) {
                        positiveListener.onPositive();
                    }
                })
                .onNegative((dialog, which) -> {
                    if (negativeListener != null) {
                        negativeListener.onNegative();
                    }
                })
                .onNeutral((dialog, which) -> {
                    if (neutralListener != null) {
                        neutralListener.onNeutral();
                    }
                })
                .dismissListener(dialog -> {
                    if (dismissListener != null) {
                        dismissListener.onDismiss();
                    }
                })
                .items(items)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
                        if (listener != null) {
                            listener.onItemClick(which);
                        }

                        if (autoClose) {
                            dismiss();
                        }
                    }
                })
                .build();

        updateButtons();
    }

    public void setItems(CharSequence... items) {
        setItems(Arrays.asList(items));
    }

    public void setItems(@ArrayRes int itemsRes) {
        setItems(context.getResources().getTextArray(itemsRes));
    }

    public void setItems(List<CharSequence> items) {
        this.items = items;
        if (dialog != null) {
            dialog.setItems((CharSequence[]) items.toArray());
        }
    }

    public void setAutoClose(boolean autoClose) {
        this.autoClose = autoClose;
    }

    public void setItemListener(Listener listener) {
        this.listener = listener;
    }

    public interface Listener {
        void onItemClick(int position);
    }
}
