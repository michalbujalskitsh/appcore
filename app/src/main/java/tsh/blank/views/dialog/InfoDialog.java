package tsh.blank.views.dialog;

import android.content.Context;

import tsh.blank.injection.annotation.ContextActivity;
import tsh.blank.injection.annotation.PerActivity;

import javax.inject.Inject;

@PerActivity
public class InfoDialog extends BaseDialog {
    @Inject
    public InfoDialog(@ContextActivity Context context) {
        super(context);
        isCancelable = true;
        title.setText("Info");
        content.setText("stub");
        neutralText.setText("NEUTRAL");
        negativeText.setText("CANCEL");
        positiveText.setText("OK");
        isNeutralVisible = false;
        isNegativeVisible = false;
        isPositiveVisible = true;
    }

    @Override
    protected void build() {
        buildDefault();
    }
}

