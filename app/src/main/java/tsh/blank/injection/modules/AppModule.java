package tsh.blank.injection.modules;

import android.app.Application;
import android.content.Context;

import com.orhanobut.hawk.Hawk;
import com.orhanobut.hawk.HawkBuilder;

import org.greenrobot.eventbus.EventBus;

import dagger.Module;
import dagger.Provides;
import tsh.blank.injection.annotation.ContextApp;
import tsh.blank.injection.annotation.PerApp;
import tsh.blank.session.HawkPreferences;
import tsh.blank.session.PersistentSource;

@Module
public class AppModule {

    private final Application application;

    public AppModule(Application application) {
        this.application = application;
    }

    @Provides
    @PerApp
    Application providesApplication() {
        return application;
    }

    @Provides
    @ContextApp
    Context providesContext() {
        return application;
    }

    @Provides
    @PerApp
    EventBus providesEventBus() {
        return EventBus.getDefault();
    }

    @Provides
    @PerApp
    PersistentSource providesPersistentSource() {
        HawkBuilder builder = Hawk.init(application);
        return new HawkPreferences(builder);
    }
}
