package tsh.blank.injection.modules;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.rafakob.utils.network.TrustManagerSelfSigned;

import org.threeten.bp.LocalDate;
import org.threeten.bp.LocalTime;
import org.threeten.bp.OffsetDateTime;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import tsh.blank.BuildConfig;
import tsh.blank.injection.annotation.PerApp;
import tsh.blank.session.HttpInterceptorAuthorization;
import tsh.blank.session.HttpSession;
import tsh.blank.common.utils.converters.LocalDateConverter;
import tsh.blank.common.utils.converters.LocalTimeConverter;
import tsh.blank.common.utils.converters.OffsetDateTimeConverter;

@Module
public class NetworkModule {
    public NetworkModule() {
    }

    @Provides
    @PerApp
    SSLSocketFactory providesSslSocketFactory() {
        SSLSocketFactory sslSocketFactory = null;
        try {
            SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, new TrustManager[]{new TrustManagerSelfSigned()}, new SecureRandom());
            sslSocketFactory = sslContext.getSocketFactory();
        } catch (NoSuchAlgorithmException | KeyManagementException e) {
            e.printStackTrace();
        }
        return sslSocketFactory;
    }

    @Provides
    @PerApp
    HttpInterceptorAuthorization providesAuthorizationInterceptor(HttpSession session) {
        return new HttpInterceptorAuthorization(session);
    }

    @Provides
    @PerApp
    HttpLoggingInterceptor providesLoggingInterceptor() {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE);
        return loggingInterceptor;
    }


    @Provides
    @PerApp
    OkHttpClient providesOkHttpClient(SSLSocketFactory sslSocketFactory,
                                      HttpLoggingInterceptor loggingInterceptor,
                                      HttpInterceptorAuthorization authorization) {
        return new OkHttpClient.Builder()
                .sslSocketFactory(sslSocketFactory, new TrustManagerSelfSigned())
                .hostnameVerifier((s, sslSession) -> true)
                .addInterceptor(authorization)
                .addInterceptor(loggingInterceptor)
                .build();
    }

    @Provides
    @PerApp
    Gson providesGson() {
        return new GsonBuilder()
                .registerTypeAdapter(LocalTime.class, new LocalTimeConverter())
                .registerTypeAdapter(LocalDate.class, new LocalDateConverter())
                .registerTypeAdapter(OffsetDateTime.class, new OffsetDateTimeConverter())
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();
    }

    @Provides
    @PerApp
    public Retrofit providesRetrofit(OkHttpClient okHttpClient, Gson gson) {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.API_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(okHttpClient)
                .build();
    }


}
