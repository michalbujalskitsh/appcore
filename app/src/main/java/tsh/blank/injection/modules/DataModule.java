package tsh.blank.injection.modules;

import android.content.Context;

import com.github.pwittchen.prefser.library.Prefser;

import dagger.Module;
import dagger.Provides;
import tsh.blank.injection.annotation.ContextApp;
import tsh.blank.injection.annotation.PerApp;


@Module
public class DataModule {

    @Provides
    @PerApp
    Prefser providesPrefser(@ContextApp Context context) {
        return new Prefser(context);
    }
}
