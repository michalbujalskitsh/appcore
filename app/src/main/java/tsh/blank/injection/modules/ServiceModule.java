package tsh.blank.injection.modules;

import android.content.Context;
import android.support.v4.app.NotificationManagerCompat;
import tsh.blank.core.BaseDaggerService;
import tsh.blank.injection.annotation.ContextActivity;
import tsh.blank.injection.annotation.PerActivity;
import tsh.blank.injection.annotation.PerApp;

import dagger.Module;
import dagger.Provides;

/**
 * Base module for all activities.
 * Use it when you need to provide only activity context.
 */
@Module
public class ServiceModule {
    protected final BaseDaggerService service;

    public ServiceModule(BaseDaggerService service) {
        this.service = service;
    }

    @Provides
    @PerApp
    BaseDaggerService providesService() {
        return service;
    }

    @Provides
    @PerActivity
    @ContextActivity
    Context providesContext() {
        return service;
    }

    @Provides
    @PerApp
    NotificationManagerCompat providesNotificationManager(){
        return NotificationManagerCompat.from(service);
    }
}
