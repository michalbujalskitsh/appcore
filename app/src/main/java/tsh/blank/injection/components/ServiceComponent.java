package tsh.blank.injection.components;

import tsh.blank.injection.annotation.PerApp;
import tsh.blank.injection.modules.ServiceModule;

import dagger.Subcomponent;

@PerApp
@Subcomponent(modules = {ServiceModule.class, })
public interface ServiceComponent {

}