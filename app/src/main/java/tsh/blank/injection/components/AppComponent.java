package tsh.blank.injection.components;


import dagger.Component;
import tsh.blank.injection.annotation.PerApp;
import tsh.blank.injection.modules.AppModule;
import tsh.blank.injection.modules.DataModule;
import tsh.blank.injection.modules.NetworkModule;
import tsh.blank.ui.MainSomethingComponent;
import tsh.blank.ui.MainSomethingModule;

@PerApp
@Component(modules = {AppModule.class, DataModule.class, NetworkModule.class})
public interface AppComponent {

    MainSomethingComponent with(MainSomethingModule mainSomethingModule);

}