package tsh.blank.core;

import android.app.Activity;
import android.content.Context;
import android.net.wifi.WifiManager;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import tsh.blank.R;

public class MvpViewDelegate {
    private final Context context;
    private final CoordinatorLayout coordinatorLayout;
    private final RecyclerView recyclerView;
    private final View contentView;
    private final View emptyView;
    private final View progressView;


    public MvpViewDelegate(Activity context) {
        this.context = context;
        this.coordinatorLayout = (CoordinatorLayout) context.findViewById(R.id.coordinator_layout);
        this.recyclerView = (RecyclerView) context.findViewById(R.id.recycler_view);
        this.contentView = context.findViewById(R.id.content_view);
        this.emptyView = context.findViewById(R.id.empty_view);
        this.progressView = context.findViewById(R.id.progress_view);

    }

    public MvpViewDelegate(Activity context, View view) {
        this.context = context;
        this.coordinatorLayout = (CoordinatorLayout) view.findViewById(R.id.coordinator_layout);
        this.recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        this.contentView = view.findViewById(R.id.content_view);
        this.emptyView = view.findViewById(R.id.empty_view);
        this.progressView = view.findViewById(R.id.progress_view);

    }

    public void showContent() {
        if (progressView != null) progressView.setVisibility(View.GONE);
        if (emptyView != null) emptyView.setVisibility(View.GONE);
        if (contentView != null) contentView.setVisibility(View.VISIBLE);
        if (recyclerView != null) recyclerView.setVisibility(View.VISIBLE);
    }

    public void showProgressView() {
        if (progressView != null) progressView.setVisibility(View.VISIBLE);
        if (emptyView != null) emptyView.setVisibility(View.GONE);
        if (contentView != null) contentView.setVisibility(View.GONE);
        if (recyclerView != null) recyclerView.setVisibility(View.GONE);
    }

    public void showEmptyView() {
        if (progressView != null) progressView.setVisibility(View.GONE);
        if (emptyView != null) emptyView.setVisibility(View.VISIBLE);
        if (contentView != null) contentView.setVisibility(View.GONE);
        if (recyclerView != null) recyclerView.setVisibility(View.GONE);
    }

    public void showSnackbarIndefinite(String msg) {
        if (coordinatorLayout != null) {
            Snackbar.make(coordinatorLayout, msg, Snackbar.LENGTH_INDEFINITE)
                    .setAction("OK", v -> {
                    })
                    .show();
        }
        showContent();
    }

    public void showSnackbarShort(String msg) {
        Snackbar.make(coordinatorLayout, msg, Snackbar.LENGTH_SHORT).show();
    }


    public void showSnackbarLong(String msg) {
        Snackbar.make(coordinatorLayout, msg, Snackbar.LENGTH_LONG).show();
    }


    public void showToastShort(String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public void showToastLong(String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public boolean isWifiEnabled() {
        return ((WifiManager) context.getSystemService(Context.WIFI_SERVICE)).isWifiEnabled();
    }

    public CoordinatorLayout getCoordinatorLayout() {
        return coordinatorLayout;
    }
}
