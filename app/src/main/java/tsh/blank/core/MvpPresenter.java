package tsh.blank.core;

public interface MvpPresenter<VIEW extends MvpView> {

    void bindView(VIEW view);

    void unbindView();

    boolean isViewBound();
}
