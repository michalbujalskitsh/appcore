package tsh.blank.core;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import butterknife.ButterKnife;
import tsh.blank.R;

public abstract class MvpFragment<PRESENTER extends MvpPresenterImpl> extends BaseFragment implements MvpView {
    public MvpViewDelegate delegate;
    public RecyclerView recyclerView;
    public View progressView;
    @Inject protected PRESENTER presenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (presenter != null) {
            presenter.bindView(this);
        }
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (presenter != null) {
            presenter.bindView(this);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (presenter != null && !presenter.isViewBound()) {
            presenter.bindView(this);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (presenter != null) {
            presenter.unbindView();
        }
    }

    public void setContentView(View view) {
        delegate = new MvpViewDelegate(getActivity(), view);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        progressView = view.findViewById(R.id.progress_view);
        ButterKnife.bind(this, view);
    }

    @Override
    public void showContent() {
        delegate.showContent();
    }

    @Override
    public void showProgressView() {
        delegate.showProgressView();
    }

    @Override
    public void showEmptyView() {
        delegate.showEmptyView();
    }

    @Override
    public void showSnackbarIndefinite(String message) {
        delegate.showSnackbarIndefinite(message);
    }

    @Override
    public void showSnackbarShort(String message) {
        delegate.showSnackbarShort(message);
    }

    @Override
    public void showSnackbarLong(String message) {
        delegate.showSnackbarLong(message);
    }

    @Override
    public void showToastShort(String message) {
        delegate.showToastShort(message);
    }

    @Override
    public void showToastLong(String message) {
        delegate.showToastLong(message);
    }

    @Override
    public boolean isWifiEnabled() {
        return delegate.isWifiEnabled();
    }
}
