package tsh.blank.core;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import javax.inject.Inject;

import butterknife.ButterKnife;
import tsh.blank.R;

public abstract class MvpActivity<PRESENTER extends MvpPresenter> extends BaseActivity implements MvpView {
    @Inject protected PRESENTER presenter;
    protected MvpViewDelegate delegate;
    protected RecyclerView recyclerView;
    protected View progressView;
    protected Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (presenter != null) {
            presenter.bindView(this);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (presenter != null && !presenter.isViewBound()) {
            presenter.bindView(this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (presenter != null && !presenter.isViewBound()) {
            presenter.bindView(this);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (presenter != null && presenter.isViewBound()) {
            presenter.unbindView();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (presenter != null) {
            presenter.unbindView();
        }
    }

    protected void setToolbar(String title, boolean backArrowEnabled) {
        if (toolbar != null) {
            toolbar.setTitle(title);
            setSupportActionBar(toolbar);
            if (backArrowEnabled) {
                setBackArrowEnabled();
            }
        }
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
        delegate = new MvpViewDelegate(this);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        progressView = findViewById(R.id.progress_view);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        ButterKnife.bind(this);
    }

    @Override
    public void showContent() {
        delegate.showContent();
    }

    @Override
    public void showProgressView() {
        delegate.showProgressView();
    }

    @Override
    public void showEmptyView() {
        delegate.showEmptyView();
    }


    @Override
    public void showSnackbarIndefinite(String message) {
        delegate.showSnackbarIndefinite(message);
    }

    @Override
    public void showSnackbarShort(String message) {
        delegate.showSnackbarShort(message);
    }

    @Override
    public void showSnackbarLong(String message) {
        delegate.showSnackbarLong(message);
    }

    @Override
    public void showToastShort(String message) {
        delegate.showToastShort(message);
    }

    @Override
    public void showToastLong(String message) {
        delegate.showToastLong(message);
    }

    @Override
    public boolean isWifiEnabled() {
        return delegate.isWifiEnabled();
    }
}
