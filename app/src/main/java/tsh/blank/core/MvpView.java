package tsh.blank.core;

public interface MvpView {
    void showContent();

    void showProgressView();

    void showEmptyView();

    void showSnackbarIndefinite(String message);

    void showSnackbarShort(String message);

    void showSnackbarLong(String message);

    void showToastShort(String message);

    void showToastLong(String message);

    boolean isWifiEnabled();
}
