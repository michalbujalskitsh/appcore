package tsh.blank.core;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.rafakob.utils.ColorUtils;

import tsh.blank.App;
import tsh.blank.R;
import tsh.blank.injection.components.AppComponent;


public abstract class BaseActivity extends AppCompatActivity {
//    private ActivityComponent activityComponent;

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupInjection();
    }

    protected abstract void setupInjection();

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    protected AppComponent getAppComponent() {
        return App.get(this).getAppComponent();
    }


    protected void setStatusBarColor(@ColorInt int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(color);
        }
    }

    protected void setBackArrowEnabled() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(false);
        }
    }

    protected void setMenuIconTint(Menu menu) {
        for (int i = 0; i < menu.size(); i++) {
            setMenuItemIconTint(menu.getItem(i));
        }
    }

    protected void setMenuItemIconTint(MenuItem menuItem) {
        menuItem.getIcon().setColorFilter(ColorUtils.getFromAttr(this, R.attr.md_divider_color), PorterDuff.Mode.SRC_IN);
    }

    protected void setMenuShowAlways(Menu menu) {
        for (int i = 0; i < menu.size(); i++) {
            setMenuItemShowAlways(menu.getItem(i));
        }
    }

    protected void setMenuItemShowAlways(MenuItem menuItem) {
        menuItem.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_ALWAYS);
    }

    protected boolean isInLandscape() {
        return getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
    }

    protected boolean isInPortrait() {
        return getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT;
    }

    public void startActivityAndClearTask(Intent intent) {
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    protected void hideSoftKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    protected boolean isServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}