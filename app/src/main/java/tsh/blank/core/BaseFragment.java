package tsh.blank.core;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import tsh.blank.App;
import tsh.blank.injection.components.AppComponent;


public abstract class BaseFragment extends Fragment {
//    private ActivityComponent activityComponent;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected AppComponent getAppComponent() {
        return App.get(getActivity()).getAppComponent();
    }

//    protected ActivityComponent getActivityComponent() {
//        if (activityComponent == null) {
//            activityComponent = getAppComponent().with(new ActivityModule(getActivity()));
//        }
//        return activityComponent;
//    }
}
