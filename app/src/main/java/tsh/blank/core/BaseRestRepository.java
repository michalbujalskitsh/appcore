package tsh.blank.core;

import retrofit2.Retrofit;

/**
 * Created by andrzej on 25.02.2016.
 */
public abstract class BaseRestRepository<RETROFIT_SERVICE> {

    protected RETROFIT_SERVICE api;
    protected Class<RETROFIT_SERVICE> type;

    public BaseRestRepository(Retrofit retrofit, Class type) {
        this.type = type;
        createApi(retrofit);
    }

    protected void createApi(Retrofit retrofit) {
        api = retrofit.create(type);
    }

}
