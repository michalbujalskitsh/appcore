package tsh.blank.core;

import android.app.Service;

import tsh.blank.App;
import tsh.blank.injection.components.AppComponent;
import tsh.blank.injection.components.ServiceComponent;

/**
 * Created by andrzej on 15.04.2016.
 */
public abstract class BaseDaggerService extends Service {

    private ServiceComponent mServiceComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        setServiceInjection();
    }

    protected AppComponent getAppComponent() {
        return App.get(this).getAppComponent();
    }

    protected abstract void setServiceInjection();

//    protected ServiceComponent inject() {
//        if (mServiceComponent == null) {
//            mServiceComponent = getAppComponent().with(new ServiceModule(this));
//        }
//        return mServiceComponent;
//    }
}
