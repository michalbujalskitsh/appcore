package tsh.blank.core;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import retrofit2.adapter.rxjava.HttpException;
import rx.subscriptions.CompositeSubscription;
import tsh.blank.data.ApiException;

public abstract class MvpPresenterImpl<VIEW extends MvpView> implements MvpPresenter<VIEW>{
    protected final CompositeSubscription rx = new CompositeSubscription();
    private WeakReference<VIEW> viewRef = null;

    public MvpPresenterImpl() {
    }

    public final void bindView(VIEW view) {
        viewRef = new WeakReference<>(view);
    }

    public void unbindView() {
        clearSubscriptions();
        viewRef = null;
    }

    protected final VIEW getView() {
        if (viewRef == null) {
            throw new NullPointerException("getView() called when viewRef is null. Ensure bindView(View view) is called first.");
        }
        return viewRef.get();
    }

    public final boolean isViewBound() {
        return viewRef != null;
    }

    public void clearSubscriptions() {
        rx.clear();
    }

    public String parseNetworkError(Throwable t) {

        if (t instanceof UnknownHostException) {
            return "No Wi-Fi connection. Please retry.";
        }

        if (t instanceof SocketTimeoutException || t instanceof SocketException) {
            return "Connection timeout.";
        }

        if (t instanceof IOException) {
            return "Connection error occurred.";
        }

        ApiException apiException = new ApiException();
        if (t instanceof HttpException) {
            try {
                HttpException httpException = (HttpException) t;
                apiException = new Gson().fromJson(httpException.response().errorBody().source().readUtf8(), ApiException.class);

                if (apiException == null) {
                    apiException = new ApiException();
                }

                apiException.setHttpCode(httpException.code());

                if (TextUtils.isEmpty(apiException.getMessage())) {
                    if (httpException.code() == 500) {
                        apiException.setMessage("Server error occurred. Please contact with admin.");
                    } else {
                        apiException.setMessage("Unexpected API error.");
                    }
                }

            } catch (IOException | JsonSyntaxException e) {
                apiException.setMessage("Unexpected API error.");
                e.printStackTrace();
            }
        }
        return apiException.getMessage();
    }
}
