package tsh.blank.session;

public class Token {
    protected String accessToken;

    public Token() {
    }

    public Token(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getAccessToken() {
        return accessToken;
    }


    public Token setAccessToken(String accessToken) {
        this.accessToken = accessToken;
        return this;
    }
}
