package tsh.blank.session;

public interface PersistentSource {
    <ENTITY> ENTITY get(String key);
    <ENTITY> void put(String key, ENTITY value);
    boolean has(String key);
    void remove(String key);
    void clear();
}
