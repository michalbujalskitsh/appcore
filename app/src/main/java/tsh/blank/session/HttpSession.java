package tsh.blank.session;

import javax.inject.Inject;

import tsh.blank.injection.annotation.PerApp;

@PerApp
public class HttpSession {

    public static final String HEADER_AUTHORIZATION = "Authorization";
    private static final String KEY_USER = "USER";
    private static final String KEY_TOKEN = "TOKEN";

    private Token lastToken;
    private AppUser loggedUser;
    private final PersistentSource source;

    @Inject
    public HttpSession(PersistentSource source) {
        this.source = source;
    }


    public void init() {
        lastToken = source.get(KEY_TOKEN);
        loggedUser = source.get(KEY_USER);
    }

    public void setAccessToken(Token token) {
        this.lastToken = token;
        source.put(KEY_TOKEN, token);
    }

    public void setLoggedUser(AppUser loggedUser) {
        this.loggedUser = loggedUser;
        source.put(KEY_USER, loggedUser);
    }

    public AppUser getLoggedUser() {
        return loggedUser;
    }

    public String getAccessToken() {
        if (lastToken == null) {
            return null;
        }
        return lastToken.getAccessToken();
    }

    public void checkOut() {
        lastToken = null;
        loggedUser = null;

        source.remove(KEY_TOKEN);
        source.remove(KEY_USER);
    }
}
