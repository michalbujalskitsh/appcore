package tsh.blank.session;

public class AppUser {
    private long id;
    private String firstName;
    private String lastName;
    private String surname;
    private String avatarUrl;

    public long getId() {
        return id;
    }

    public AppUser setId(long id) {
        this.id = id;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public AppUser setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public AppUser setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public String getSurname() {
        return surname;
    }

    public AppUser setSurname(String surname) {
        this.surname = surname;
        return this;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public AppUser setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
        return this;
    }
}
