package tsh.blank.session;

import com.orhanobut.hawk.Hawk;
import com.orhanobut.hawk.HawkBuilder;

public class HawkPreferences implements PersistentSource {

    public HawkPreferences(HawkBuilder builder) {
        builder.build();
        clear();
        Hawk.put("ALOES", "testtest");
    }

    @Override
    public <E> E get(String key) {
        return Hawk.get(key);
    }

    @Override
    public <F> void put(String key, F value) {
        Hawk.put(key, value);
    }

    @Override
    public boolean has(String key) {
        return Hawk.contains(key);
    }

    @Override
    public void remove(String key) {
        Hawk.delete(key);
    }

    @Override
    public void clear() {
        Hawk.deleteAll();
    }
}
