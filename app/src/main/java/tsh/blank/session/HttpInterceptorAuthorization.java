package tsh.blank.session;


import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class HttpInterceptorAuthorization implements Interceptor {

    private final HttpSession session;

    public HttpInterceptorAuthorization(HttpSession session) {
        this.session = session;
    }

    @Override
    public Response intercept(Interceptor.Chain chain) throws IOException {

        Request request = chain.request();

        if (session.getAccessToken() != null) {
            Request.Builder requestBuilder = request.newBuilder()
                    .header("Accept", "application/json")
                    .header(HttpSession.HEADER_AUTHORIZATION, session.getAccessToken())
                    .method(request.method(), request.body());
            request = requestBuilder.build();
        }
        return chain.proceed(request);
    }

}


