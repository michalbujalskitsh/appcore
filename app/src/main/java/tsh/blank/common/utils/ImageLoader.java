package tsh.blank.common.utils;

import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.io.File;

import jp.wasabeef.glide.transformations.CropCircleTransformation;

public class ImageLoader {
    public static void loadCenterCropCircle(ImageView imageView, String url) {
        Glide.with(imageView.getContext())
                .load(processUrl(url))
//                .placeholder(R.drawable.difolt)
                .centerCrop()
//                .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                .bitmapTransform(new CropCircleTransformation(imageView.getContext()))
                .into(imageView);
    }

    public static void loadCenterCropCircleNoAnim(ImageView imageView, String url) {
        Glide.with(imageView.getContext())
                .load(processUrl(url))
//                .placeholder(R.drawable.difolt)
                .centerCrop()
                .dontAnimate()
//                .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                .bitmapTransform(new CropCircleTransformation(imageView.getContext()))
                .into(imageView);
    }

    public static void loadCenterCrop(ImageView imageView, String url) {
        Glide.with(imageView.getContext())
                .load(processUrl(url))
//                .placeholder(R.drawable.difolt)
                .centerCrop()
//                .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                .into(imageView);
    }

    public static void loadCenterCropNoAnim(ImageView imageView, String url) {
        Glide.with(imageView.getContext())
                .load(processUrl(url))
//                .placeholder(R.drawable.difolt)
                .dontAnimate()
                .centerCrop()
//                .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                .into(imageView);
    }

    public static void loadFitCenter(ImageView imageView, String url) {
        Glide.with(imageView.getContext())
                .load(processUrl(url))
//                .placeholder(R.drawable.difolt)
                .fitCenter()
//                .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                .into(imageView);
    }

    public static void loadFitCenter(ImageView imageView, File file) {
        Glide.with(imageView.getContext())
                .load(file)
//                .placeholder(R.drawable.difolt)
//                .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                .into(imageView);
    }

    private static String processUrl(String url) {
        if(tsh.blank.BuildConfig.API_URL.contains("192.168")){
            return "https://cccsstagingbucket.s3.amazonaws.com/students/pictures/default/medium.jpg";
        }
        return url;
    }

}
