package tsh.blank.common.utils;


import tsh.blank.BuildConfig;

public class CrashUtils {
    public static void logException(Throwable throwable) {
        if (!isEnabled()) return;
//            Crashlytics.logException(throwable);
    }

    public static void logText(String text) {
        if (!isEnabled()) return;
//            Answers.getInstance().logCustom(new CustomEvent(text));
    }

    private static boolean isEnabled() {
        return BuildConfig.CRASHLYTICS_ENABLED;
    }
}
