package tsh.blank.common.utils;

import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;

import tsh.blank.injection.annotation.PerActivity;
import com.rafakob.utils.SdcardUtils;
import com.rafakob.utils.exception.SdcardException;

import java.io.File;

import javax.inject.Inject;

@PerActivity
public class Camera {
    public static final int PHOTO_REQUEST_CODE = 100;
    public static final int VIDEO_REQUEST_CODE = 200;
    public static final int TYPE_PHOTO = 0;
    public static final int TYPE_VIDEO = 1;

    @Inject
    public Camera() {
    }

    public Intent getPhotoIntent() throws SdcardException {
        return getIntent("", TYPE_PHOTO);
    }

    public Intent getVideoIntent() throws SdcardException {
        return getIntent("", TYPE_VIDEO);
    }

    private Intent getIntent(String path, int type) throws SdcardException {
        if (!SdcardUtils.isWritable()) {
            throw new SdcardException("You don't have permissions to write on external storage");
        }

        File outputFile = new File(path);
        Intent intent;

        if (type == TYPE_PHOTO) {
            intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        } else {
            intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        }

        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(outputFile));
        return intent;
    }
}