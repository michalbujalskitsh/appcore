package tsh.blank.common.utils;

import android.support.v7.widget.DefaultItemAnimator;

public class DefaultItemAnimatorNoChange extends DefaultItemAnimator {
    public DefaultItemAnimatorNoChange() {
        setSupportsChangeAnimations(false);
    }
}
