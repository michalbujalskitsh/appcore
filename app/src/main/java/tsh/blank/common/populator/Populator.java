package tsh.blank.common.populator;

import java.util.ArrayList;
import java.util.List;

//todo add rx sub based repopulating
public abstract class Populator<SOURCE, RESULT> {

	public abstract RESULT populateSingle(SOURCE s);

	public ArrayList<RESULT> populateList(List<SOURCE> sourceList) {
		ArrayList<RESULT> results = new ArrayList<>();
		for (SOURCE s : sourceList) {
			results.add(populateSingle(s));
		}
		return results;
	}
}
