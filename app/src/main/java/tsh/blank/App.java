package tsh.blank;

import android.app.Application;
import android.content.Context;
import android.support.annotation.VisibleForTesting;

import com.jakewharton.threetenabp.AndroidThreeTen;

import timber.log.Timber;
import tsh.blank.injection.components.AppComponent;
import tsh.blank.injection.components.DaggerAppComponent;
import tsh.blank.injection.modules.AppModule;

public class App extends Application {
    private AppComponent appComponent;

    public static App get(Context context) {
        return (App) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        AndroidThreeTen.init(this);
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }

        if (!BuildConfig.DEBUG && BuildConfig.CRASHLYTICS_ENABLED) {
        }
        getAppComponent();
    }

    public AppComponent getAppComponent() {
        if (appComponent == null) {
            appComponent = DaggerAppComponent.builder()
                    .appModule(new AppModule(this))
                    .build();
        }
        return appComponent;
    }

    @VisibleForTesting
    public void setComponent(AppComponent appComponent) {
        this.appComponent = appComponent;
    }
}
