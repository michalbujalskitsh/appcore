package tsh.blank.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import timber.log.Timber;
import tsh.blank.R;
import tsh.blank.core.MvpActivity;

public final class MainSomethingActivity extends MvpActivity<MainSomethingContract.Presenter> implements MainSomethingContract.View {

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, MainSomethingActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_something);
        Timber.d("Loguje: %s", presenter.getMessage());
    }

    @Override
    protected void setupInjection() {
        getAppComponent().with(new MainSomethingModule(this)).inject(this);
    }

}
