package tsh.blank.ui;

import tsh.blank.core.MvpPresenter;
import tsh.blank.core.MvpView;

public interface MainSomethingContract {
    interface View extends MvpView {

    }

    interface Presenter<View extends MvpView> extends MvpPresenter<View> {
        String getMessage();
    }
}
