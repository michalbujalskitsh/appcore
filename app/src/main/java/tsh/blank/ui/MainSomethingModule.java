package tsh.blank.ui;

import dagger.Module;
import dagger.Provides;
import tsh.blank.injection.annotation.PerActivity;

@Module
public class MainSomethingModule {

    private final MainSomethingActivity mActivity;

    public MainSomethingModule( MainSomethingActivity activity ) {
        mActivity = activity;
    }

    @Provides
    @PerActivity
    MainSomethingContract.Presenter providesPresenter(){
        return new MainSomethingPresenter2();
    }
}
