package tsh.blank.ui;


import dagger.Subcomponent;
import tsh.blank.injection.annotation.PerActivity;

@PerActivity
@Subcomponent(modules = {MainSomethingModule.class})
public interface MainSomethingComponent {
    void inject(MainSomethingActivity mainSomethingActivity);
}