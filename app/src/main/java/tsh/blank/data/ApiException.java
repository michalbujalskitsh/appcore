package tsh.blank.data;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ApiException {
    @SerializedName("message")
    private String message;
    @SerializedName("messages")
    private List<String> messages = new ArrayList<>();
    private int httpCode;

    public int getHttpCode() {
        return httpCode;
    }

    public void setHttpCode(int httpCode) {
        this.httpCode = httpCode;
    }

    public String getMessage() {
        if (TextUtils.isEmpty(message) && messages != null && !messages.isEmpty()) {
            message = "";
            for (int i = 0; i < messages.size(); i++) {
                message += messages.get(i) + "\n";
            }
            message = message.substring(0, message.length() - 1);
        }

        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<String> getMessages() {
        return messages;
    }

    public void setMessages(List<String> messages) {
        this.messages = messages;
    }

    @Override
    public String toString() {
        return getMessage();
    }
}
